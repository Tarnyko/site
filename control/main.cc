/*
 * main.cc - implementation
 * control, a collection of site control routines for
 * Adélie Web Site
 * Adélie Linux
 *
 * Copyright (c) 2018 Adélie Linux team. All rights reserved.
 * License: NCSA
 */


#include <QCoreApplication>
#include <QFileSystemWatcher>
#include <QProcess>
#include <QtGlobal>

#ifndef SRV_PATH
#	define SRV_PATH "/srv/www/adelie"
#endif

#define SITE_CHANGED SRV_PATH "/.site_updated"
#define DOCS_CHANGED SRV_PATH "/.docs_updated"

/*! Run `git pull --force` in the specified `dir`.
 * \note This function blocks.
 * \returns true if git worked, false otherwise.
 */
bool update_git(const QString &dir)
{
	QProcess *process = new QProcess;
	QStringList args;
	bool success;

	qDebug("git pull --force");
	process->setWorkingDirectory(dir);
	process->setProgram("/usr/bin/git");
	args << "pull" << "--force";
	process->setArguments(args);
	process->start();

	success = process->waitForFinished();
	delete process;
	return success;
}

int main(int argc, char *argv[])
{
        QCoreApplication app(argc, argv);

	QFileSystemWatcher *watcher = new QFileSystemWatcher;
	watcher->addPath(SITE_CHANGED);
	watcher->addPath(DOCS_CHANGED);
	QObject::connect(watcher, (void (QFileSystemWatcher:: *)(const QString&))&QFileSystemWatcher::fileChanged,
		[=](const QString &path) {
			if(path.compare(SITE_CHANGED) == 0) {
				// site changed
				qInfo("Updating site...");
				if(!update_git("/srv/www/adelie")) {
					qWarning("We appear to have failed to update the site.");
				} else {
					qInfo("Site updated.");
				}
			} else if(path.compare(DOCS_CHANGED) == 0) {
				// docs changed
				QProcess *process;
				qInfo("Updating documentation...");

				if(!update_git("/srv/www/docs")) {
					qWarning("We appear to have failed to update docs.");
					return;
				}

				process = new QProcess;
				process->setWorkingDirectory("/srv/www/docs");
				process->setProgram("/usr/bin/make");
				process->start();
				if(!process->waitForFinished()) {
					qWarning("We appear to have failed to make new docs.");
					delete process;
					return;
				}
				delete process;
				qInfo("Docs updated.");
			} else {
				qWarning("Unknown path was specified: ");
				qWarning(path.toUtf8());
			}
	});

        return app.exec();
}

"""Tiny control server for keeping Adélie sites updated from Git.

This Web server listens on a custom port and receives two possible POST
requests from GitLab (or real-git-rcmp):

    * /site_updated: site.git has been changed, update it
    * /docs_updated: docs.git has been changed, update it

This Web server does not do any updating itself.  It simply twiddles a file
that is being watched by another process, which does the actual heavy lifting.
"""


from flask import Flask
from pathlib import Path

APP = Flask(__name__)
"""The Flask app that we use."""

@APP.route('/site_updated', methods=('POST',))
def site_updated():
    """GitLab signal that site.git has one or more new commits."""
    Path("/srv/www/adelie/.site_updated").touch()
    return "OK"

@APP.route('/docs_updated', methods=('POST',))
def docs_updated():
    """GitLab signal that docs.git has one or more new commits."""
    Path("/srv/www/adelie/.docs_updated").touch()
    return "OK"

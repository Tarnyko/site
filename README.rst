==================================
 README for Adélie Linux Web Site
==================================
:Authors:
 * **A. Wilcox**, primary writer
:Status:
 Draft
:Copyright:
 © 2015-2018 Adélie Linux.  CC BY-NC-SA open source license.



Introduction
============

This distribution contains the Web site for Adélie Linux.


License
```````
The Web site itself is licensed under a
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

The fonts are licensed under the Open Font License.

The CSS is licensed under the CC-BY 2.0 license.

The ``control/`` directory is licensed under the NCSA license.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.
